<?php

namespace Drupal\pexels_library\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\pexels_library\PexelsLibraryClient;
use Drupal\pexels_library\SearchParameters;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\Psr7\Response;

/**
 * Configure Pexels Library settings for this site.
 */
class PexelsSettingsForm extends ConfigFormBase {

  /**
   * Media pexels client http.
   *
   * @var \Drupal\pexels_library\PexelsLibraryClient
   */
  protected $client;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Api rate limit state array.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $apiRateLimit;

  /**
   * Search parameters data.
   *
   * @var \Drupal\pexels_library\SearchParameters
   */
  protected $searchParameters;

  /**
   * Create PexelsSettingsForm.
   *
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    MessengerInterface $messenger,
    PexelsLibraryClient $pexels_client,
    StateInterface $api_rate_limit,
    SearchParameters $search_parameters
  ) {
    parent::__construct($config_factory);
    $this->client = $pexels_client;
    $this->messenger = $messenger;
    $this->apiRateLimit = $api_rate_limit;
    $this->searchParameters = $search_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('pexels_library.client'),
      $container->get('state'),
      $container->get('pexels_library.search_parameters'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pexels_library_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['pexels_library.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['pexels_config'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Pexels configuration'),
    ];
    $form['pexels_config']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Authorization Pexels api key'),
      '#default_value' => $this->config('pexels_library.settings')->get(
        'api_key'
      ),
    ];
    $form['pexels_config']['photo_config'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Photo settings default'),
    ];
    $form['pexels_config']['photo_config']['photo_orientation'] = [
      '#type' => 'select',
      '#title' => $this->t('Orientation photo'),
      '#options' => $this->combineOptions(
        $this->searchParameters->getOrientation()
      ),
      '#default_value' => $this->config('pexels_library.settings')->get(
        'photo_orientation'
      ),
      '#empty_option' => $this->t('- None -'),
    ];
    $form['pexels_config']['photo_config']['photo_size'] = [
      '#type' => 'select',
      '#title' => $this->t('Size photo'),
      '#options' => $this->combineOptions(
        $this->searchParameters->getPhotoSize()
      ),
      '#default_value' => $this->config('pexels_library.settings')->get(
        'photo_size'
      ),
      '#empty_option' => $this->t('- None -'),
    ];
    $form['pexels_config']['photo_config']['photo_color'] = [
      '#type' => 'select',
      '#title' => $this->t('Color photo'),
      '#options' => $this->combineOptions(
        $this->searchParameters->getPhotoColor()
      ),
      '#default_value' => $this->config('pexels_library.settings')->get(
        'photo_color'
      ),
      '#empty_option' => $this->t('- None -'),
    ];
    $form['pexels_config']['photo_config']['photo_locale'] = [
      '#type' => 'select',
      '#title' => $this->t('locale language photo'),
      '#options' => $this->combineOptions($this->searchParameters->getLocale()),
      '#default_value' => $this->config('pexels_library.settings')->get(
        'photo_locale'
      ),
      '#empty_option' => $this->t('- None -'),
    ];
    $form['pexels_config']['video_config'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Video settings default'),
    ];
    $form['pexels_config']['video_config']['video_orientation'] = [
      '#type' => 'select',
      '#title' => $this->t('Orientation video'),
      '#options' => $this->combineOptions(
        $this->searchParameters->getOrientation()
      ),
      '#default_value' => $this->config('pexels_library.settings')->get(
        'video_orientation'
      ),
      '#empty_option' => $this->t('- None -'),
    ];
    $form['pexels_config']['video_config']['video_size'] = [
      '#type' => 'select',
      '#title' => $this->t('Size video'),
      '#options' => $this->combineOptions(
        $this->searchParameters->getVideoSize()
      ),
      '#default_value' => $this->config('pexels_library.settings')->get(
        'video_size'
      ),
      '#empty_option' => $this->t('- None -'),
    ];
    $form['pexels_config']['video_config']['video_locale'] = [
      '#type' => 'select',
      '#title' => $this->t('Local language video'),
      '#options' => $this->combineOptions($this->searchParameters->getLocale()),
      '#default_value' => $this->config('pexels_library.settings')->get(
        'video_locale'
      ),
      '#empty_option' => $this->t('- None -'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $api_key = $form_state->getValue('api_key');
    if ($api_key === 'api_key') {
      $form_state->setErrorByName(
        'api_key',
        $this->t('The value is not correct.')
      );
    }
    else {
      $response = $this->client->verifyAuthorization($api_key);
      if ($response instanceof Response && $response->getStatusCode() === 200) {
        // Set Rate limits state.
        $api_rate_limit = [
          'pexels.rate_limit' => $response->getHeaderLine('X-Ratelimit-Limit'),
          'pexels.rate_remainig' => $response->getHeaderLine(
            'X-Ratelimit-Remaining'
          ),
          'pexels.rate_reset' => $response->getHeaderLine('X-Ratelimit-Reset'),
        ];
        $this->apiRateLimit->setMultiple($api_rate_limit);
        $this->messenger->addMessage($this->t('Pexels connection success'));
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('pexels_library.settings')
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('photo_orientation', $form_state->getValue('photo_orientation'))
      ->set('photo_size', $form_state->getValue('photo_size'))
      ->set('photo_color', $form_state->getValue('photo_color'))
      ->set('photo_locale', $form_state->getValue('photo_locale'))
      ->set('video_orientation', $form_state->getValue('video_orientation'))
      ->set('video_size', $form_state->getValue('video_size'))
      ->set('video_locale', $form_state->getValue('video_locale'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Transform array to key value and translated value.
   *
   * @param array $array
   *   List.
   *
   * @return array
   *   Value list.
   */
  public function combineOptions(array $array): array {
    $return = [];
    foreach ($array as $value) {
      $value = ucfirst($value);
      $return[$value] = $this->t($value);
    }
    return $return;
  }

}
