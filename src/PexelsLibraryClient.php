<?php

namespace Drupal\pexels_library;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

/**
 * Media Pexels client.
 */
class PexelsLibraryClient {

  /**
   * Photo base url.
   */
  protected const PHOTO_BASE_URL = "https://api.pexels.com/v1/";

  /**
   * Video base url.
   */
  protected const VIDEO_BASE_URL = "https://api.pexels.com/videos/";

  /**
   * Collection base url.
   */
  protected const COLLECTION_BASE_URL = "https://api.pexels.com/v1/collections/";

  /**
   * A config object to retrieve Pexels config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Authorization api key.
   *
   * @var string
   */
  protected $apiKey;

  /**
   * Client http.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * Pexels Api constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('pexels_library.settings');
    $this->apiKey = $this->config->get('api_key');
    $this->client = new Client([
      'base_uri' => self::PHOTO_BASE_URL,
      'headers' => [
        'Authorization' => $this->apiKey,
      ],
    ]
    );
  }

  /**
   * Verify authorization key api.
   *
   * @return \Psr\Http\Message\ResponseInterface|string
   *   Return response.
   */
  public function verifyAuthorization() {
    try {
      $request = new Request(
        'GET', self::PHOTO_BASE_URL . 'search?query=people'
      );
      return $this->client->send($request);
    }
    catch (\Throwable $e) {
      return $e->getMessage();
    }
  }

  /**
   * Get rate limits.
   */
  public function getRateLimits() {
    // @todo get rate limits from states
  }

}
