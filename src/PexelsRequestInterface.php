<?php

namespace Drupal\pexels_library;

/**
 * Request Interface.
 */
interface PexelsRequestInterface {

  /**
   * Get the list of orientation for photo and video.
   *
   * @return array
   *   Orientation.
   */
  public function getOrientation(): array;

  /**
   * Get the list of allowed photo size.
   *
   * @return array
   *   Allowed photo size.
   */
  public function getPhotoSize(): array;

  /**
   * Get the list of allowed video size.
   *
   * @return array
   *   Allowed video size.
   */
  public function getVideoSize():array;

  /**
   * Get the list of photo color.
   *
   * @return array
   *   Allowed photo color.
   */
  public function getPhotoColor(): array;

  /**
   * Get list of locale search language.
   *
   * @return array
   *   Allowed locale.
   */
  public function getLocale(): array;

}
