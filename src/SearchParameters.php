<?php

namespace Drupal\pexels_library;

/**
 * Search Parameters.
 */
class SearchParameters implements PexelsRequestInterface {

  /**
   * {@inheritdoc}
   */
  public function getOrientation(): array {
    return [
      'landscape',
      'portrait',
      'square',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getPhotoSize(): array {
    return [
      'large',
      'medium',
      'square',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getPhotoColor(): array {
    return [
      'red',
      'orange',
      'yellow',
      'green',
      'turquoise',
      'blue',
      'violet',
      'pink',
      'brown',
      'black',
      'gray',
      'white',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getLocale(): array {
    return [
      'en-US',
      'pt-BR',
      'es-ES',
      'ca-ES',
      'de-DE',
      'it-IT',
      'fr-FR',
      'sv-SE',
      'id-ID',
      'pl-PL',
      'ja-JP',
      'zh-TW',
      'zh-CN',
      'ko-KR',
      'th-TH',
      'nl-NL',
      'hu-HU',
      'vi-VN',
      'cs-CZ',
      'da-DK',
      'fi-FI',
      'uk-UA',
      'el-GR',
      'ro-RO',
      'nb-NO',
      'sk-SK',
      'tr-TR',
      'ru-RU',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getVideoSize(): array {
    return [
      'large',
      'medium',
      'small',
    ];
  }

}
